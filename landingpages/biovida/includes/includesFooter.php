<!-- jQuery Scripts -->
<script src="../js/jquery.min.js"></script>
<script src="../bootstrap.min.js"></script>
<script src="../js/plugins.js"></script>
<script src="../js/scripts.js"></script>
<script src="../js/validation.js"></script>
<script src="../js/api-estado-cidade.js"></script>
<script src="../js/FormSteps.js"></script>
<script src="../js/jquery.validate.min.js"></script>
<script src="../js/custom.js"></script>

<!-- Cookies -->
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script src="../js/cookies.js"></script>
