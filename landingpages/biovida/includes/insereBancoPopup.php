<?php

if(isset($_POST['cadastrar-exit'])){
    $strEmail            = trim(strip_tags($_POST['strEmail']));
    $strCidade           = trim(strip_tags($_POST['strCidade']));


    $insert = "INSERT INTO popupsaida ( strEmail, strCidade ) VALUES ( :strEmail, :strCidade )";
    try{

        $result = $conexao->prepare($insert);

        $result->bindParam(':strEmail', $strEmail, PDO::PARAM_STR);
        $result->bindParam(':strCidade', $strCidade, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                $msgClientesSucesso = '
                                                        <script type="text/javascript">
                                    window.location = "obrigado-saida.php";
                                                </script>';

            }
        }else{
            $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erro</strong> ao cadastrar o usuário.
                    </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>