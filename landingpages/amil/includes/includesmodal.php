<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Zona Oeste</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
                <li> Uni Butantã</li>
                <li>Hosp. E Mat. Jardins</li>
                <li>Hosp. Metropolitano</li>
                <li>Itamaraty Rebolsas</li>
              <li>  São Camilo Pompéia</li>
                <li>Samaritano</li>
                <li>São Luiz Morumbi  </li>
</ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Zona Norte</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
              <li>Hosp. Presidente</li>
              <li>Hosp. Nipo Brasileiro</li>
              <li>São Camilo</li>
</ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Centro</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
              <li>Cruz Azul</li>
              <li>Bandeirantes</li>
              <li>Adventista</li>
              <li>Sta. Isabel</li>
              <li>Igesp</li>
              <li>Total Cor</li>
              <li>Paulistano Bela Vista</li>
            <li>  Hosp. Infantil Sabará</li>
            <li>  Santa Catarina</li>
              <li>Nove de Julho</li>
              <li>Oswaldo Cruz</li>
            <li>  Pró Matre</li>
</ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Zona Leste</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
            <li>  Paranaguá</li>
            <li>  Day Hosp. Ermelino Matarazzo</li>
            <li>  Hosp. C Guaianases - Vl. Iolanda</li>
            <li>  Hosp. E Casa De Saúde Sta. Marcelina</li>
            <li>  Hosp. E Mat. Oito De Maio</li>
            <li>  Hosp. E Mat. São Miguel</li>
            <li>  Hosp. Sto. Expedito - Somel</li>
            <li>  Hosp. São Carlos - Vl. Matilde</li>
            <li>  Hosp. Vitória - Anália Franco</li>
            <li>  Hosp. Vitória - Un. Avançada Tatuapé</li>
            <li>  Ibcc - Ins. Brasileiro De Controle Do Câncer</li>
            <li>  Sta. Marcelina</li>
            <li>  Cema</li>
            <li>  Hosp. Central de Guaianases</li>
            </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Zona Sul</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
            <li>  AACD</li>
            <li>  Alvorada em Moema</li>
            <li>  Hosp. Da Luz</li>
            <li>  Hosp. Do Rim</li>
            <li>  Hosp. De Olhos</li>
            <li>  Hosp. Paulistano</li>
            <li>  Ruben Berta</li>
            <li>  São Rafael</li>
          <li>    Cruz Vermelha (Deformidades da Face)</li>
            <li>  Hosp. Da Criança</li>
            <li>  Hosp. Paulista de Otorrino.</li>
            <li>  Sta. Cruz</li>
            <li>  Sta. Paula</li>
            <li>  Sta. Rita</li>
            <li>  São Camilo</li>
            <li>  Hosp. Da Unifesp</li>
            <li>  Bosque da Saúde</li>
            <li>  Sta. Joana</li>
            <li>  São Luiz</li>
            <li>  Leforte</li>
</ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Zona ABCD</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">

            <li>  Hosp. Vital Mauá H/PS</li>
            <li>  São Bernardo do Campo</li>
            <li>  Hosp. ABC - SBC Uni. Cirúrgica H/PS</li>
            <li>  Hosp. ABC - SBC Uni. Materno Infantil H/M/OS</li>
            <li>  Hosp. Abc - Un. Avançada Diadema Cto. - A/PA</li>
          <li>    Hosp. Sta. Casa De Mauá - H/M/OS</li>
            <li>  Hosp. Ribeirão Pires - H/M/OS</li>
            <li>  Hosp. Abc - Un. Avançada Sto. André – PA</li>
            <li>  Hosp. E Mat. Bartira - H/M/OS</li>
          <li>    Hosp. Abc - Sbc Un. Cirúrgica - H/OS</li>
            <li>  Hosp. Abc - Sbc Un. Materno-infantil - H/M/OS</li>
            <li>  Hosp. Abc - Un. Avançada São Caetano – A</li>
            <li>  Hosp. E Mat. América- Mauá- H/M/PS</li>
            <li>  Hosp. Vital- Mauá H/OS</li>
            <li>  Hosp. Ben. Portuguesa de Sto André Hosp São Pedro H/M/OS</li>
            <li>  Hosp. E Mat. Dr. Christóvão da Gama H/M/OS</li>
            <li>  Hosp. E Mat. Assunção H/OS</li>
            <li>  Hosp. Bene. Portuguesa De Sto. André - Hosp. São Pedro</li>
          <li>  Hosp. E Mat. Bartira - M/PS</li>
</ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Região do Alto do Tiête </h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
                <li>Arujá</li>
              <li>Hosp. Ama - Lions Clube- Arujá</li>

            <li>  Guarulhos</li>
              <li>Hosp. Carlos Chagas</li>
            <li>  Casa De Saúde De Guarulhos </li>
            <li>  Hosp. Carlos Chagas</li>
            <li>  Hosp. Stella Maris Guarulhos</li>
            <li>  Hosp. Bom Clima Guarulhos</li>
            <li>  Hosp. Casa De Saúde Guarulhos</li>
            <li>  Hosp. Stella Maris Guarulhos</li>
            <li>  Hosp. Carlos Chagas</li>
            <li>  Hosp. Casa De Saúde Guarulhos</li>
            <li>  Hosp. Stella Maris Guarulhos</li>

            <li>  Mogi das Cruzes</li>
            <li>  Hosp. Ipiranga - Mogi das Cruzes</li>
            <li>  Hosp. E Mat. Mogi Dor</li>
            <li>  Hosp. Santana Mogi das Cruzes H/PS</li>
            <li>  Hosp. AMA Arujá</li>
            <li> </li>

    </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Região de  Itapecerica da Serra</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
                <li>Taboão da Serra</li>
  <li>Family Hosp. - Semear</li>

  <li>Cotia</li>
  <li>Hosp. São Francisco - Cotia</li>


    </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Região de Osasco</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">
                <li>Osasco</li>
                <li>Hosp. Sino Brasileiro - H/M/PS</li>
                <li>Hosp. Nsa. Senhora De Fátima - Osasco - H/PS</li>
              <li>  Hosp. Metropolitano - Un. Osasco - A/PA</li>

              <li>  Itapevi</li>
              <li>  Hosp. Cruzeiro Do Sul - Itapevi - PS</li>


    </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Região de Franco da Rocha</h4>
            </div>
            <div class="modal-body">
              <ul style="font-size:15px">

          <li> Caieiras</li>
  <li> Hosp. das Clinicas Caieiras - H/M/PS</li>

  <li> Cajamar</li>
  <li> Pronto Atendimento Sta. Elisa - PA</li>

  </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
